﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;

namespace SiMay.Core.ScreenSpy
{
    public class ScreenCaptureHelper
    {
        [DllImport("gdi32.dll")]
        private static extern bool BitBlt(
            IntPtr hdcDest,
            int nXDest,
            int nYDest,
            int nWidth,
            int nHeight,
            IntPtr hdcSrc,
            int nXSrc,
            int nYSrc,
            System.Int32 dwRop
        );

        [DllImport("user32.dll", EntryPoint = "GetDesktopWindow", CharSet = CharSet.Auto, SetLastError = true)]
        private static extern IntPtr GetDesktopWindow();

        [DllImport("User32.dll")]
        private static extern IntPtr GetWindowDC(IntPtr hwnd);

        [DllImport("User32.dll")]
        private static extern int ReleaseDC(IntPtr hWnd, IntPtr hDC);

        [DllImport("user32.dll")]
        static extern IntPtr GetDC(IntPtr ptr);
        [DllImport("gdi32.dll")]
        static extern int GetDeviceCaps(
        IntPtr hdc, // handle to DC  
                        int nIndex // index of capability  
                        );

        const int HORZRES = 8;
        const int VERTRES = 10;
        const int LOGPIXELSX = 88;
        const int LOGPIXELSY = 90;
        const int DESKTOPVERTRES = 117;
        const int DESKTOPHORZRES = 118;

        public static Bitmap CaptureNoCursor()
        {
            IntPtr deskDeviceContext = GetWindowDC(GetDesktopWindow());

            Graphics g1 = Graphics.FromHdc(deskDeviceContext);

            //int screenHeight = GetSystemMetrics(SM_CYSCREEN)+100;
            //int screenWidth = GetSystemMetrics(SM_CXSCREEN)+500;

            int screenHeight = GetDeviceCaps(deskDeviceContext, DESKTOPVERTRES);
            int screenWidth = GetDeviceCaps(deskDeviceContext, DESKTOPHORZRES);

            Bitmap MyImage = new Bitmap(screenWidth, screenHeight, g1);

            Graphics g2 = Graphics.FromImage(MyImage);

            IntPtr destdeviceContext = g2.GetHdc();
            IntPtr sourcedeviceContext = g1.GetHdc();

            BitBlt(destdeviceContext, 0, 0, screenWidth, screenHeight, sourcedeviceContext, 0, 0, 13369376);

            //释放dc,否则dc句柄过多会导致程序崩溃
            ReleaseDC(IntPtr.Zero, deskDeviceContext);

            g2.ReleaseHdc(destdeviceContext);
            g1.ReleaseHdc(sourcedeviceContext);

            g1.Dispose();
            g2.Dispose();

            return MyImage;
        }
    }
}
