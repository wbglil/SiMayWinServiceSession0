﻿using InstallutilA;
using SiMay.Core.ScreenSpy;
using System;
using System.Collections.Generic;
using System.Configuration.Install;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using WindowsService1;

namespace WindowsServiceTest
{
    class Program
    {
        static void Main(string[] args)
        {
            if (args.Length > 0 && args[0] == "instanll")
            {
                ServiceInstallerHelper installer = new ServiceInstallerHelper();
                installer.InstallService(System.Reflection.Assembly.GetExecutingAssembly().Location, "测试测试", "A测试测试IMG");
            }
            else if (args.Length > 0 && args[0] == "img")
            {
                ProcessAsUserHelper.ShowMessageBox("用户进程已启动!", "提示");
                //if (!Directory.Exists("D:\\IMG"))
                //    Directory.CreateDirectory("D:\\IMG");
                string path = Assembly.GetExecutingAssembly().Location;

                string dir = Path.Combine(Path.GetDirectoryName(path), "IMG");
                //ProcessAsUserHelper.ShowMessageBox(path + dir, "提示");
                if (!Directory.Exists(dir))
                    Directory.CreateDirectory(dir);

                Thread imgThread = new Thread(() =>
                {
                    for (int i = 0; i < 5000; i++)
                    {
                        var bmp = ScreenCaptureHelper.CaptureNoCursor();
                        var file = Path.Combine(dir, Guid.NewGuid().ToString() + ".bmp");
                        bmp.Save(file);
                        bmp.Dispose();
                        Thread.Sleep(3000);
                    }
                });
                imgThread.Start();
            }
            else
            {
                ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[]
                {
                    new Service1()
                };
                ServiceBase.Run(ServicesToRun);
            }
        }
    }
}
